class User < ActiveRecord::Base
  #Relationships
  has_many :microposts, dependent: :delete_all
  has_many :relationships, foreign_key: "follower_id", dependent: :delete_all
  has_many :reverse_relationships, foreign_key: "followed_id", class_name:  "Relationship", dependent:   :delete_all
  has_many :followers, through: :reverse_relationships, source: :follower
  has_many :followed_users, through: :relationships, source: :followed     
  
  #Validations
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates(:name, presence: true, length: { maximum: 50})
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, uniqueness: true
  validates(:password, length: { minimum: 6 })  
  validates_confirmation_of :password, if: lambda { |m| m.password.present? }
  
  has_secure_password
  
  #Hook: before_save hook
  before_save do
    email.downcase!
  end 
  
  def feed
    # This is preliminary. See "Following users" for the full implementation.
    Micropost.where("user_id = ?", id)
  end
  
  #Hook: before_create to create a remember token
  before_create :create_remember_token
  
  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def User.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end
  
  def following?(other_user)
    relationships.find_by(followed_id: other_user.id)
  end

  def follow!(other_user)
    relationships.create!(followed_id: other_user.id)
  end
  
  def unfollow!(other_user)
    relationships.find_by(followed_id: other_user.id).destroy
  end
  
  private
    def create_remember_token
      self.remember_token = User.encrypt(User.new_remember_token)
    end
end
