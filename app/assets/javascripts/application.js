// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require turbolinks
//= require_tree .

$(document).ready(function () {
	$("#user_name").focus();
	
	$('#micropost_content').keyup (function () {
		var amount = $(this).val().length;
		var amount_to_go = 140 - amount;
		
		if(amount >= 140) {
			$(this).addClass('red_color');
		}
		else {
			$(this).removeClass('red_color');
		}
		
		$("#micropost_content").val();
		$("#micropost_content_amount").val(amount_to_go + " characters to go");
	});
});

