class SessionsController < ApplicationController
  before_action :signed_in_user, only: [:create, :new]
  
  def destroy
    sign_out
    redirect_to root_url
  end
  
  def new
  end
  
  def create
    user = User.find_by(email: params[:email].downcase)
    if !user
      flash.now[:error] = 'Unknown email. Please try again'
      render 'new'
      return
    end
    
    
    if user && user.authenticate(params[:password])
      sign_in user
      redirect_back_or user
    else
      flash.now[:error] = 'Invalid email/password combination. Please try again'
      render 'new' 
    end
  end
  
  private 
  def signed_in_user
    if signed_in?
      redirect_to root_url
    end
  end
end
