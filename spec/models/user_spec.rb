require 'spec_helper'

describe User do
  before do 
    @user = User.new(name: "Ron Spierings", email: "ron@r-spierings.nl", password: "ronisthebest", password_confirmation: "ronisthebest")
  end
  
  subject { @user }
  it { should respond_to(:email) }
  it { should respond_to(:name) }
  it { should respond_to(:password_digest) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:remember_token) }
  it { should respond_to(:authenticate) }
  it { should respond_to(:admin) }
  it { should respond_to(:admin) }
  it { should respond_to(:microposts) }
  it { should respond_to(:feed) }
  it { should respond_to(:relationships) }
  it { should respond_to(:followed_users) }
  it { should respond_to(:unfollow!) }
  it { should respond_to(:reverse_relationships) }
  it { should respond_to(:followers) }
  it { should be_valid }
  
  it "Should respond to 'name'" do
    expect(@user).to respond_to(:name)
  end
  
  describe "should not be valid on empty name" do
    before { @user.name = "" }
    it {should_not be_valid }    
  end
  
  describe "should not be valid on empty email" do 
    before { 
        @user.email = "" 
      }
    it {should_not be_valid}
  end
  
  describe "name should not be longer than 10 characters" do
    before {
      @user.name = "a" * 52
      }
    it {should_not be_valid}
  end
  
  describe "when email format is invalid" do
    it "should be invalid" do
      addresses = %w[user@foo,com user_at_foo.org example.user@foo. foo@bar_baz.com foo@bar+baz.com]
      addresses.each do |invalid_address|
        @user.email = invalid_address
        expect(@user).not_to be_valid
      end
    end
    
  end
  
  describe "when email format is valid" do
    it "should be valid" do
      addresses = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
      addresses.each do |valid_address|
        @user.email = valid_address
        expect(@user).to be_valid
      end      
    end
  end
  
  describe "should not be able to create duplicate email" do
    before do
      duplicate_user = @user.dup
      duplicate_user.save    
    end
    
    it { 
      should_not be_valid
    }    
  end
  
  describe "should not be able to duplicate email UPPERCASE TOO" do
    before do
      duplicate_user = @user.dup
      duplicate_user.email = @user.email.upcase
      duplicate_user.save
    end
    
    it {
      should_not be_valid
    }
  end
  
  
 
  describe "when password is empty" do
    before do
      @user = User.new(name: "Ron S", email: "ron@r-spierings.nl", password: " ", password_confirmation: " ")
    end
    it {should_not be_valid }
  end
  
  describe "when passwords do not match" do
    before do
      @user = User.new(name: "Ron Spierings", email: "ron@r-spierings.nl", password: " mis", password_confirmation: "match")
    end
    it do
      should_not be_valid
    end    
  end
  
  describe "return value of authenticate method" do
    before do
      @user.save
    end
    let(:found_user) do
      User.find_by(email: @user.email)
    end
    describe "with valid password" do
      it do
        should eq found_user.authenticate(@user.password)
      end
    end
    
    describe "with invalid password" do
      let(:invalid_user) do
        found_user.authenticate("invalid password")
      end
      it do
        should_not eq invalid_user
      end
      specify do
        expect(invalid_user).to be_false
      end
    end
    
    describe "with a password that's too short" do
      before do
        @user.password = @user.password_confirmation = "a" * 5
      end
      it do
        should_not be_valid
      end
    end
  end

  describe "Excersice| " do
    #Excercise 1 : Check if email is actually downcased
    describe "should not be able to insert uppercase into DB" do
      before do
        @user.email = @user.email.upcase
        @user.save
      end
      it do
        expect(@user.email).to_not match(@user.email.upcase)
      end    
    end
  end
  
  
  describe "edit" do
    subject { page }
    let(:user) { FactoryGirl.create(:user) }
    before do
      sign_in user
      visit edit_user_path(user)
    end

    describe "page" do
      it { should have_content("Update your profile") }
      it { should have_title("Edit user") }
      it { should have_link('change', href: 'http://gravatar.com/emails') }
    end

    describe "with invalid information" do
      before { click_button "Save changes" }

      it { should have_content('error') }
    end
    
    describe "with valid information" do
      let(:new_name)  { "New Name" }
      let(:new_email) { "new@example.com" }
      before do
        fill_in "Name",             with: new_name
        fill_in "Email",            with: new_email
        fill_in "Password",         with: user.password
        fill_in "Confirm Password", with: user.password
        click_button "Save changes"
      end

      it { should have_title(new_name) }
      it { should have_selector('div.alert.alert-success') }
      it { should have_link('Sign out', href: signout_path) }
      specify { expect(user.reload.name).to  eq new_name }
      specify { expect(user.reload.email).to eq new_email }
    end 
  end
  
  describe "with admin attribute set to 'true'" do
    before do
      @user.save!
      @user.toggle!(:admin)
    end

    it { should be_admin }
  end
  
  describe "micropost associations" do
    before { @user.save }
    let!(:older_micropost) do
      FactoryGirl.create(:micropost, user: @user, created_at: 1.day.ago)
    end
    let!(:newer_micropost) do
      FactoryGirl.create(:micropost, user: @user, created_at: 1.hour.ago)
    end

    it "should have the right microposts in the right order" do
      expect(@user.microposts.to_a).to eq [newer_micropost, older_micropost]
    end
    
    it "should destroy associated microposts" do
      microposts = @user.microposts.to_a
      @user.destroy
      expect(microposts).not_to be_empty
      microposts.each do |micropost|
        expect(Micropost.where(id: micropost.id)).to be_empty
      end
    end
    
    describe "status" do
      let(:unfollowed_post) do
        FactoryGirl.create(:micropost, user: FactoryGirl.create(:user))
      end

      its(:feed) { should include(newer_micropost) }
      its(:feed) { should include(older_micropost) }
      its(:feed) { should_not include(unfollowed_post) }
    end
  end
  
  describe "profile page" do 
    subject { page }
    let(:user) { FactoryGirl.create(:user) }
    let!(:m1) { FactoryGirl.create(:micropost, user: user, content: "Foo") }
    let!(:m2) { FactoryGirl.create(:micropost, user: user, content: "Bar") }

    before { visit user_path(user) }

    it { should have_content(user.name) }
    it { should have_title(user.name) }

    describe "microposts" do
      it { should have_content(m1.content) }
      it { should have_content(m2.content) }
      it { should have_content(user.microposts.count) }
    end
  end
  
  
  describe "following" do
    let(:other_user) { FactoryGirl.create(:user) }
    before do
      @user.save
      @user.follow!(other_user)
    end

    it { should be_following(other_user) }
    its(:followed_users) { should include(other_user) }
    
    describe "followed user" do
      subject { other_user }
      its(:followers) { should include(@user) }
    end
  
    describe "and unfollowing" do
      before { @user.unfollow!(other_user) }

      it { should_not be_following(other_user) }
      its(:followed_users) { should_not include(other_user) }
    end
  
  end
end