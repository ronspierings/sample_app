require 'spec_helper'

describe "User Pages" do
  subject { page }
  
  before do
    @user = User.new(name: "Example User", email: "user@example.com", password: "foobar", password_confirmation: "foobar")
  end
  
  describe "index" do
    before do
      sign_in FactoryGirl.create(:user)
      FactoryGirl.create(:user, name: "Bob", email: "bob@example.com")
      FactoryGirl.create(:user, name: "Ben", email: "ben@example.com")
      visit users_path
    end

    it { should have_title('All users') }
    it { should have_content('All users') }

    it "should list each user" do
      User.all.each do |user|
        expect(page).to have_selector('li', text: user.name)
      end
    end
    
   describe "delete links" do

      it { should_not have_link('delete') }

      describe "as an admin user" do
        let(:admin) { FactoryGirl.create(:admin) }
        before do
          sign_in admin
          visit users_path
        end
      end
      
      describe "as non-admin user" do
        let(:user) { FactoryGirl.create(:user) }
        let(:non_admin) { FactoryGirl.create(:user) }
  
        before { sign_in non_admin, no_capybara: true }
  
        describe "submitting a DELETE request to the Users#destroy action" do
          before { delete user_path(user) }
          specify { expect(response).to redirect_to(root_path) }
        end
      end
    end
  end
  
 
  describe "Signup page" do
    before {visit signup_path}
    
    it { should have_content('Sign up') }
    it { should have_title(full_title('Sign up')) }
  end
  
  describe "signup" do
    before do
      visit signup_path
    end
    let (:submit) do
      "Create my account"
    end
    
    describe "with invalid  information" do
      it "should not create a user" do
        expect do
          click_button submit.not_to change(User, :count)
        end
      end
    end
    
    describe "with valid information" do
      before do
        fill_in "Name",         with: "Ron Spierings"
        fill_in "Email",        with: "ron@r-SPIERINGS.nl"
        fill_in "Password",     with: "123456"
        fill_in "Confirmation", with: "123456"
     end
     it "should create a user" do
       expect {click_button submit}.to change(User, :count).by(1)
     end
    end
  end
  
  
  describe "remember token" do
    subject { @user }
    before { @user.save }
    its(:remember_token) { should_not be_blank }
  end
  
  
   describe "following/followers" do
    let(:user) { FactoryGirl.create(:user) }
    let(:other_user) { FactoryGirl.create(:user) }
    before { user.follow!(other_user) }

    describe "followed users" do
      before do
        sign_in user
        visit following_user_path(user)
      end

      it { should have_title(full_title('Following')) }
      it { should have_selector('h3', text: 'Following') }
      it { should have_link(other_user.name, href: user_path(other_user)) }
    end

    describe "followers" do
      before do
        sign_in other_user
        visit followers_user_path(other_user)
      end

      it { should have_title(full_title('Followers')) }
      it { should have_selector('h3', text: 'Followers') }
      it { should have_link(user.name, href: user_path(user)) }
    end
  end
end
