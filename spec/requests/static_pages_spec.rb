require 'spec_helper'

describe "Static pages | " do
  
  subject { page }
   
  describe "Home - Should have a Hero unit + text + no title" do
    before { visit root_path }
    
    it { should have_selector('.hero-unit', text: 'Welcome to the Sample App') }
    it { should_not have_title('| Home') }    
    
    describe "for signed-in users" do
      let(:user) { FactoryGirl.create(:user) }
      before do
        FactoryGirl.create(:micropost, user: user, content: "Lorem ipsum")
        FactoryGirl.create(:micropost, user: user, content: "Dolor sit amet")
        sign_in user
        visit root_path
      end

      it "should render the user's feed" do
        user.feed.each do |item|
          expect(page).to have_selector("li##{item.id}", text: item.content)
        end
      end
      
      describe "follower/following counts" do
        let(:other_user) { FactoryGirl.create(:user) }
        before do
          other_user.follow!(user)
          visit root_path
        end

        it { should have_link("0 following", href: following_user_path(user)) }
        it { should have_link("1 followers", href: followers_user_path(user)) }
      end
    end
  end


  describe "Help page - Should have blockquotes and content| " do 
    before { visit help_path }
    
    it { should have_selector('blockquote') }
    it { should have_content('With A Little Help From My Friends') }
  end
  
  describe "About page - Page and content" do
    before {visit about_path}
    
    it { should have_title('| About')}
    it { should_not have_selector('h1', text: 'Home')}
    it { should have_selector('p', text:'What about me?!?')}
  end
end